<?php
class AccessModifier{
    public function TestPublic(){
        echo "this is from a public function";
    }
    protected function TestProtected(){
        echo "this is from a protected function which can  be accessed from this class and it's subclass";
    }
    private function TestPrivate(){
        echo "this is from a private function which can only be accessed from this class";
    }


    public function AccessPrivate(){

    }
}