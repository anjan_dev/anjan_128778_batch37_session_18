<?php
class TestCall
{
    public function __call($name, $arguments)
    {
        // Note: value of $name is case sensitive.
        echo $name."</br>";
    }


    static function __callStatic($name, $arguments)
    {
        echo $name."</br>";
    }
}

$obj = new TestCall;
$obj->wrongPublicName('in object context');
TestCall::wrongStaticMethodName();
